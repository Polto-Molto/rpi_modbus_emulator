#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QModbusDataUnit>
#include <QSerialPort>
#include <QModbusPdu>
#include <QModbusRequest>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //0102025900016861
    static const char cmdBuf[] = { 0x01, 0x02, 0x02, 0x59 ,0x00 ,0x01 ,0x68, 0x60 };
    QByteArray byteArray = QByteArray::fromRawData(cmdBuf, sizeof(cmdBuf));

    QModbusPdu mypdu;
    mypdu.setData(byteArray);
    mypdu.setFunctionCode(QModbusPdu::WriteSingleRegister);
    QModbusRequest request(QModbusRequest::WriteMultipleCoils,
                            QByteArray::fromHex("0102025900016861"));
    QModbusRequest request2(mypdu);

    qDebug() << request2.data();
    qDebug() << request2.calculateDataSize(request2);
    qDebug() << request.calculateDataSize(request);
    qDebug() << request.data();
    qDebug() << request.isValid();

    master = new QModbusRtuSerialMaster;
    master->setConnectionParameter(QModbusDevice::SerialPortNameParameter , "/dev/"+ui->comboBox->currentText());
    master->setConnectionParameter(QModbusDevice::SerialBaudRateParameter,9600);
    master->setConnectionParameter(QModbusDevice::SerialDataBitsParameter,8);
    master->setConnectionParameter(QModbusDevice::SerialParityParameter,0);
    master->setConnectionParameter(QModbusDevice::SerialStopBitsParameter,1);
   // master->setNumberOfRetries(0);
//    master->setInterFrameDelay(5000000);
   // master->setTimeout(2000);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_connect_clicked()
{
    master->connectDevice();
}

void Widget::on_pushButton_601_clicked()
{
   // data = QModbusDataUnit(QModbusDataUnit::DiscreteInputs, 1537, 128);
   // rply = master->sendReadRequest(data ,ui->spinBox->value());
    QModbusRequest request(QModbusRequest::WriteMultipleCoils,
         QByteArray::fromHex("07010080291e"));
    master->sendRawRequest(request,1);
   // connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

void Widget::on_pushButton_701_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::DiscreteInputs, 1793, 128);
    rply = master->sendReadRequest(data ,ui->spinBox->value());
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

void Widget::on_pushButton_001_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 1, 8);
    rply = master->sendReadRequest(data ,ui->spinBox->value());
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

void Widget::on_pushButton_011_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 17, 8);
    rply = master->sendReadRequest(data ,ui->spinBox->value());
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

void Widget::on_pushButton_021_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 33, 8);
    rply = master->sendReadRequest(data ,ui->spinBox->value());
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}

void Widget::on_pushButton_401_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 1025, 64);
    rply = master->sendReadRequest(data ,ui->spinBox->value());
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}


void Widget::printData()
{
    ui->label_status->setText(rply->errorString());

    int i =0;
    QModbusDataUnit::RegisterType type = rply->result().registerType();
    switch(type)
    {
    case QModbusDataUnit::Coils:
        break;

    case QModbusDataUnit::DiscreteInputs:
        if(rply->result().startAddress() == 1537 )
        {
            ui->textEdit_601->clear();
            foreach (quint8 value , rply->result().values()) {
                ui->textEdit_601->append(QString::number(1537+i,16)+"  :  " + QString::number(value));
            i++;
            }
        }
        if(rply->result().startAddress() == 1793 )
        {
            ui->textEdit_701->clear();
            foreach (quint8 value , rply->result().values()) {
                ui->textEdit_701->append(QString::number(1793+i,16)+"  :  " +QString::number(value));
                i++;
            }
        }

        break;

    case QModbusDataUnit::InputRegisters:
        break;

    case QModbusDataUnit::HoldingRegisters:
        if(rply->result().startAddress() == 1 )
        {
            ui->textEdit_01->clear();
            foreach (quint16 value , rply->result().values()) {
                ui->textEdit_01->append(QString::number(1+i,16)+"  :  " +QString::number(value));
                i++;
            }
        }

        if(rply->result().startAddress() == 17 )
        {
            ui->textEdit_11->clear();
            foreach (quint16 value , rply->result().values()) {
                ui->textEdit_11->append(QString::number(17+i,16)+"  :  "+ QString::number(value));
                i++;
            }
        }

        if(rply->result().startAddress() == 33 )
        {
            ui->textEdit_21->clear();
            foreach (quint16 value , rply->result().values()) {
                ui->textEdit_21->append(QString::number(33+i,16)+"  :  " +QString::number(value));
                i++;
            }
        }

        if(rply->result().startAddress() == 1025 )
        {
            ui->textEdit_401->clear();
            foreach (quint16 value , rply->result().values()) {
                ui->textEdit_401->append(QString::number(1025+i,16)+"  :  " +QString::number(value));
                i++;
            }
        }
        break;
    }
}


/*
void Widget::on_pushButton_set_data_clicked()
{
    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 0, 2);
    rply = master->sendReadRequest(data ,2);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));

    QVector<quint16> values;
    values << 50 << 60 ;

    data = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, 0, values);
    rply = master->sendWriteRequest(data ,15);
    connect(rply,SIGNAL(finished()) , this ,SLOT(printData()));
}
*/
