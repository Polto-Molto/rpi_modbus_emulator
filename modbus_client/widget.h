#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QModbusRtuSerialMaster>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
    void on_pushButton_connect_clicked();
    void on_pushButton_601_clicked();
    void on_pushButton_701_clicked();
    void on_pushButton_001_clicked();
    void on_pushButton_011_clicked();
    void on_pushButton_021_clicked();
    void on_pushButton_401_clicked();
    void printData();

private:
    Ui::Widget *ui;
    QModbusRtuSerialMaster * master;
    QModbusDataUnit data;
    QModbusReply * rply= NULL;
};

#endif // WIDGET_H
