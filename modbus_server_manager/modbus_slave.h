#ifndef MODBUS_SLAVE_H
#define MODBUS_SLAVE_H

#include <QObject>
#include <QModbusRtuSerialSlave>
#include <QDebug>
#include <QThread>

class modbus_slave : public QModbusRtuSerialSlave
{
    Q_OBJECT
public:
    explicit modbus_slave(QObject *parent = 0);

    void setConnectionValues(QString port,int buadrate,int data_bits
                             ,int stop_bits,int _parity);

    void setModbusMemoryMap(quint16 coil__start,quint16 coil__end
                            ,quint16 discrete__start ,quint16 discrete__end
                            ,quint16 input__start,quint16 input__end
                            ,quint16 holding__start,quint16 holding__end);

signals:

public slots:
    void func1(QModbusDevice::State);
    void func2(QModbusDataUnit::RegisterType,int,int);
    void manipulateError(QModbusDevice::Error error);

private:


public:
    QString port_name;
    int baudRate;
    int databits;
    int stopbits;
    int parity;

    quint16 coil_start;
    quint16 coil_end;

    quint16 discrete_start;
    quint16 discrete_end;

    quint16 holding_start;
    quint16 holding_end;

    quint16 input_start;
    quint16 input_end;
};

#endif // MODBUS_SLAVE_H
