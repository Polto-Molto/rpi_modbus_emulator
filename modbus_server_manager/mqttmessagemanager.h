#ifndef MQTTMESSAGEMANAGER_H
#define MQTTMESSAGEMANAGER_H

#include <QObject>
#include <modbus_slave.h>
#include <qtmosquitto.h>
#include <QTimer>

class mqttMessageManager : public QObject
{
    Q_OBJECT
public:
    explicit mqttMessageManager(QObject *parent = 0);
    void setModbusVectorPointer(QVector<modbus_slave*> *);
    void setMqttConnectionData(QString ip ,int port , QString id);
    void setSubScripTopic(QString);

    QBitArray quint16ToQBitArray(quint16 v);
    quint16 qBitArrayToQuint16(const QBitArray &ba);

    void connectToBroker();

signals:

public slots:
    void manipulateMessage(QString,QString);
    void manipulateModbusMessage(QString);
    void heart_process();

private:
    int *mid;
    QtMosquitto* _mosq=NULL;
    QString _ip;
    int _port;
    QString _id;
    QVector<modbus_slave*> * _modbuses = NULL;
    QTimer *timer;
    int live_time;
    QString modbus_topic;
};

#endif // MQTTMESSAGEMANAGER_H
