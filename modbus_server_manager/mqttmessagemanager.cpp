#include "mqttmessagemanager.h"
#include <QBitArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>

mqttMessageManager::mqttMessageManager(QObject *parent) : QObject(parent)
{
    mid = new int;
    mosqpp::lib_init();
    live_time = 0;
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()) , this ,SLOT(heart_process()) );
    timer->start(5000);
}

void mqttMessageManager::setModbusVectorPointer(QVector<modbus_slave*> *m)
{
    _modbuses = m;
}

void mqttMessageManager::setMqttConnectionData(QString ip ,int port , QString id)
{
    _ip = ip;
    _port = port;
    _id = id;
}

void mqttMessageManager::connectToBroker()
{
    _mosq = new QtMosquitto(_id,false);
    _mosq->QmqSubscribe(mid,"heart",0);
    connect(_mosq,SIGNAL(stringTopicMessageReceived(QString,QString)),this,SLOT(manipulateMessage(QString,QString)));
    _mosq->QmqConnect_async(_ip,_port);
    _mosq->QmqLoop_start();
}

void mqttMessageManager::setSubScripTopic(QString topic)
{
    modbus_topic = topic;
    _mosq->QmqSubscribe(mid,topic,0);
    _mosq->QmqSubscribe(mid,"heart",0);

}

void mqttMessageManager::heart_process()
{
    _mosq->QmqPublish(mid,"heart","live");
    live_time++;
    if(live_time >= 2)
    {
        qDebug() << " Broker Stopped from about : " << live_time * 5 << "Seconds ..." ;
//        _mosq->QmqDisconnect();
        if(_mosq->QmqReconnect()== 0)
        {
            _mosq->QmqSubscribe(mid,"heart",0);
            _mosq->QmqSubscribe(mid,modbus_topic,0);
            _mosq->QmqPublish(mid,"heart","live");
            live_time = 0 ;
        }
    }
}

void mqttMessageManager::manipulateMessage(QString topic,QString message)
{
    if(topic == "heart")
    {
        qDebug() << "MQTT Broker is Live ... ";
        live_time = 0;
    }
    else
      manipulateModbusMessage(message);
}

void mqttMessageManager::manipulateModbusMessage(QString message)
{
    QJsonDocument doc = QJsonDocument::fromJson(message.toUtf8());
    QString raw_message = doc.object().take("data").toString();
    QByteArray bytes = QByteArray::fromBase64(raw_message.toUtf8());
    qDebug() << doc << "message: "<< message.toUtf8();
    if(raw_message != "" && bytes.length() == 5)
    {
    qDebug() << bytes ;

   quint8 modbus_address = (quint8)bytes.at(0);

   int unit_address = (int)bytes.at(1);
   QBitArray unit_state(8);
   for(int b = 0; b < 8; b++)
        unit_state.setBit( b, bytes.at(2) & (1 << (7 - b)) );

   quint16 count_low_high = (bytes.at(3) << 8 ) | (bytes.at(4)& 0xff);

   // for Discrete input registers 0x02
   quint16 address_discrete_0X601 = (unit_address * 2 )+1536; // 0x601 = 1537
   quint16 address_discrete_0X602 = (unit_address * 2 )+1537;
   quint16 address_discrete_0X701 = (unit_address * 2 )+1792; // 0x701 = 1893
   quint16 address_discrete_0X702 = (unit_address * 2 )+1793;

   quint16 address_discrete_0X801 = (unit_address * 4 )+2048; // 0x801 = 2048
   quint16 address_discrete_0X802 = (unit_address * 4 )+2049;
   quint16 address_discrete_0X803 = (unit_address * 4 )+2050;
   quint16 address_discrete_0X804 = (unit_address * 4 )+2051;


   quint16 value_0X601;
   quint16 value_0X602;
   quint16 value_0X701;
   quint16 value_0X702;

   quint16 value_0X801;
   quint16 value_0X802;
   quint16 value_0X803;
   quint16 value_0X804;

   if(unit_state[4] == 0) value_0X601 = 0; else value_0X601 =1;
   if(unit_state[5] == 0) value_0X602 = 0; else value_0X602 =1;
   if(unit_state[6] == 0) value_0X701 = 0; else value_0X701 =1;
   if(unit_state[7] == 0) value_0X702 = 0; else value_0X702 =1;

   if(unit_state[0] == 0) value_0X801 = 0; else value_0X801 =1;
   if(unit_state[1] == 0) value_0X802 = 0; else value_0X802 =1;
   if(unit_state[2] == 0) value_0X803 = 0; else value_0X803 =1;
   if(unit_state[3] == 0) value_0X804 = 0; else value_0X804 =1;

   // for holding registers 0x03
   int quotient = (int) (unit_address /8);
   int left_over = (int) (unit_address %8);

   int quotient_4 = (int) (unit_address /4);
   int left_over_4 = (int) (unit_address %4);

   int quotient_16 = (int) (unit_address /16);
   int left_over_16 = (int) (unit_address %16);

   /*
    Addr 0x601 to 0x604 “Valve Faults” Tx00 - Tx63 (Tx00 as LSB in 0x601 ...)
    Addr 0x611 to 0x614 “Float Alarms” Tx00 - Tx63 (Tx00 as LSB in 0x611...)
    Addr 0x621 to 0x624 “Valve Open Time Warnings”” Tx00 - Tx63 (Tx00 as LSB in 0x621...)
    Addr 0x631 to 0x634 "Valve Fluctuation Warnings” Tx00 - Tx63 (Tx00 as LSB in 0x631...)
    Addr 0x641 to 0x644 “Low Bat Warnings” Tx00 - Tx63 (Tx00 as LSB in 0x641...)
    Addr 0x651 to 0x654 “Heartbeats” Tx00 - Tx63 (Tx00 as LSB in 0x651...)
    */
   
   int address_holding_0X01 = quotient + 0;   // 0x01 = 1
   int address_holding_0X11 = quotient + 16;  // 0x11 = 17
   int address_holding_0X21 = quotient + 32;  // 0x21 = 33
   int address_holding_0X401 = unit_address + 1024;  // 0x401 = 1025
   int address_holding_0X501 = quotient_4 + 1280;  // 0x501 = 1281

   int address_holding_0X601 = quotient_16 + 1536;      // Addr 0x601
   int address_holding_0X602 = quotient_16 + 1537;      // Addr 0x602
   int address_holding_0X603 = quotient_16 + 1538;      // Addr 0x603
   int address_holding_0X604 = quotient_16 + 1539;      // Addr 0x604

   int address_holding_0X611 = quotient_16 + 1552;      // Addr 0x611
   int address_holding_0X612 = quotient_16 + 1553;      // Addr 0x612
   int address_holding_0X613 = quotient_16 + 1554;      // Addr 0x613
   int address_holding_0X614 = quotient_16 + 1555;      // Addr 0x614

   int address_holding_0X621 = quotient_16 + 1568;      // Addr 0x621
   int address_holding_0X622 = quotient_16 + 1569;      // Addr 0x622
   int address_holding_0X623 = quotient_16 + 1570;      // Addr 0x623
   int address_holding_0X624 = quotient_16 + 1571;      // Addr 0x624

   int address_holding_0X631 = quotient_16 + 1584;      // Addr 0x631
   int address_holding_0X632 = quotient_16 + 1585;      // Addr 0x632
   int address_holding_0X633 = quotient_16 + 1586;      // Addr 0x633
   int address_holding_0X634 = quotient_16 + 1587;      // Addr 0x634

   int address_holding_0X641 = quotient_16 + 1600;      // Addr 0x641
   int address_holding_0X642 = quotient_16 + 1601;      // Addr 0x642
   int address_holding_0X643 = quotient_16 + 1602;      // Addr 0x643
   int address_holding_0X644 = quotient_16 + 1603;      // Addr 0x644

   int address_holding_0X651 = quotient_16 + 1616;      // Addr 0x651
   int address_holding_0X652 = quotient_16 + 1617;      // Addr 0x652
   int address_holding_0X653 = quotient_16 + 1618;      // Addr 0x653
   int address_holding_0X654 = quotient_16 + 1619;      // Addr 0x654

   quint16 *holding_0X01_val = new quint16;
   quint16 *holding_0X11_val = new quint16;
   quint16 *holding_0X21_val = new quint16;
   quint16 *holding_0X501_val = new quint16;

   quint16 *holding_0X601_val = new quint16;
   quint16 *holding_0X602_val = new quint16;
   quint16 *holding_0X603_val = new quint16;
   quint16 *holding_0X604_val = new quint16;

   quint16 *holding_0X611_val = new quint16;
   quint16 *holding_0X612_val = new quint16;
   quint16 *holding_0X613_val = new quint16;
   quint16 *holding_0X614_val = new quint16;

   quint16 *holding_0X621_val = new quint16;
   quint16 *holding_0X622_val = new quint16;
   quint16 *holding_0X623_val = new quint16;
   quint16 *holding_0X624_val = new quint16;

   quint16 *holding_0X631_val = new quint16;
   quint16 *holding_0X632_val = new quint16;
   quint16 *holding_0X633_val = new quint16;
   quint16 *holding_0X634_val = new quint16;

   quint16 *holding_0X641_val = new quint16;
   quint16 *holding_0X642_val = new quint16;
   quint16 *holding_0X643_val = new quint16;
   quint16 *holding_0X644_val = new quint16;

   quint16 *holding_0X651_val = new quint16;
   quint16 *holding_0X652_val = new quint16;
   quint16 *holding_0X653_val = new quint16;
   quint16 *holding_0X654_val = new quint16;

   foreach (modbus_slave *slave , *_modbuses)
       if(slave->serverAddress() == modbus_address)
       {
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X01,holding_0X01_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X11,holding_0X11_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X21,holding_0X21_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X501,holding_0X501_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X601,holding_0X601_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X602,holding_0X602_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X603,holding_0X603_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X604,holding_0X604_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X611,holding_0X611_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X612,holding_0X612_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X613,holding_0X613_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X614,holding_0X614_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X621,holding_0X621_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X622,holding_0X622_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X623,holding_0X623_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X624,holding_0X624_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X631,holding_0X631_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X632,holding_0X632_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X633,holding_0X633_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X634,holding_0X634_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X641,holding_0X641_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X642,holding_0X642_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X643,holding_0X643_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X644,holding_0X644_val);

           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X651,holding_0X651_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X652,holding_0X652_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X653,holding_0X653_val);
           slave->data(QModbusDataUnit::HoldingRegisters,address_holding_0X654,holding_0X654_val);

       }

   QBitArray holding_0X01_bits(16);
   QBitArray holding_0X11_bits(16);
   QBitArray holding_0X21_bits(16);
   QBitArray holding_0X501_bits(16);

   QBitArray holding_0X601_bits(16);
   QBitArray holding_0X602_bits(16);
   QBitArray holding_0X603_bits(16);
   QBitArray holding_0X604_bits(16);

   QBitArray holding_0X611_bits(16);
   QBitArray holding_0X612_bits(16);
   QBitArray holding_0X613_bits(16);
   QBitArray holding_0X614_bits(16);

   QBitArray holding_0X621_bits(16);
   QBitArray holding_0X622_bits(16);
   QBitArray holding_0X623_bits(16);
   QBitArray holding_0X624_bits(16);

   QBitArray holding_0X631_bits(16);
   QBitArray holding_0X632_bits(16);
   QBitArray holding_0X633_bits(16);
   QBitArray holding_0X634_bits(16);

   QBitArray holding_0X641_bits(16);
   QBitArray holding_0X642_bits(16);
   QBitArray holding_0X643_bits(16);
   QBitArray holding_0X644_bits(16);

   QBitArray holding_0X651_bits(16);
   QBitArray holding_0X652_bits(16);
   QBitArray holding_0X653_bits(16);
   QBitArray holding_0X654_bits(16);

   holding_0X01_bits = quint16ToQBitArray(*holding_0X01_val);
   holding_0X11_bits = quint16ToQBitArray(*holding_0X11_val);
   holding_0X21_bits = quint16ToQBitArray(*holding_0X21_val);
   holding_0X501_bits = quint16ToQBitArray(*holding_0X501_val);

   holding_0X601_bits = quint16ToQBitArray(*holding_0X601_val);
   holding_0X602_bits = quint16ToQBitArray(*holding_0X602_val);
   holding_0X603_bits = quint16ToQBitArray(*holding_0X603_val);
   holding_0X604_bits = quint16ToQBitArray(*holding_0X604_val);

   holding_0X611_bits = quint16ToQBitArray(*holding_0X611_val);
   holding_0X612_bits = quint16ToQBitArray(*holding_0X612_val);
   holding_0X613_bits = quint16ToQBitArray(*holding_0X613_val);
   holding_0X614_bits = quint16ToQBitArray(*holding_0X614_val);

   holding_0X621_bits = quint16ToQBitArray(*holding_0X621_val);
   holding_0X622_bits = quint16ToQBitArray(*holding_0X622_val);
   holding_0X623_bits = quint16ToQBitArray(*holding_0X623_val);
   holding_0X624_bits = quint16ToQBitArray(*holding_0X624_val);

   holding_0X631_bits = quint16ToQBitArray(*holding_0X631_val);
   holding_0X632_bits = quint16ToQBitArray(*holding_0X632_val);
   holding_0X633_bits = quint16ToQBitArray(*holding_0X633_val);
   holding_0X634_bits = quint16ToQBitArray(*holding_0X634_val);

   holding_0X641_bits = quint16ToQBitArray(*holding_0X641_val);
   holding_0X642_bits = quint16ToQBitArray(*holding_0X642_val);
   holding_0X643_bits = quint16ToQBitArray(*holding_0X643_val);
   holding_0X644_bits = quint16ToQBitArray(*holding_0X644_val);

   holding_0X651_bits = quint16ToQBitArray(*holding_0X651_val);
   holding_0X652_bits = quint16ToQBitArray(*holding_0X652_val);
   holding_0X653_bits = quint16ToQBitArray(*holding_0X653_val);
   holding_0X654_bits = quint16ToQBitArray(*holding_0X654_val);


   holding_0X01_bits[left_over*2] = unit_state[4];
   holding_0X01_bits[left_over*2+1] = unit_state[5];

   holding_0X11_bits[left_over*2] = unit_state[4];
   holding_0X11_bits[left_over*2+1] = unit_state[5];

   holding_0X21_bits[left_over*2] = unit_state[6];
   holding_0X21_bits[left_over*2+1] = unit_state[7];


   holding_0X501_bits[left_over_4  *4] = unit_state[0];
   holding_0X501_bits[left_over_4  *4+1] = unit_state[1];
   holding_0X501_bits[left_over_4  *4+2] = unit_state[2];
   holding_0X501_bits[left_over_4  *4+3] = unit_state[3];

   if(quotient_16 == 0){
       holding_0X601_bits[left_over_16] = unit_state[4];
       holding_0X611_bits[left_over_16] = unit_state[5];
       holding_0X621_bits[left_over_16] = unit_state[0];
       holding_0X631_bits[left_over_16] = unit_state[1];
       holding_0X641_bits[left_over_16] = unit_state[2];
       holding_0X651_bits[left_over_16] = unit_state[3];
   }
   else if(quotient_16 == 1){
       holding_0X602_bits[left_over_16] = unit_state[4];
       holding_0X612_bits[left_over_16] = unit_state[5];
       holding_0X622_bits[left_over_16] = unit_state[0];
       holding_0X632_bits[left_over_16] = unit_state[1];
       holding_0X642_bits[left_over_16] = unit_state[2];
       holding_0X652_bits[left_over_16] = unit_state[3];
   }
   else if(quotient_16 == 2){
       holding_0X603_bits[left_over_16] = unit_state[4];
       holding_0X613_bits[left_over_16] = unit_state[5];
       holding_0X623_bits[left_over_16] = unit_state[0];
       holding_0X633_bits[left_over_16] = unit_state[1];
       holding_0X643_bits[left_over_16] = unit_state[2];
       holding_0X653_bits[left_over_16] = unit_state[3];
   }
   else if(quotient_16 == 3){
       holding_0X604_bits[left_over_16] = unit_state[4];
       holding_0X614_bits[left_over_16] = unit_state[5];
       holding_0X624_bits[left_over_16] = unit_state[0];
       holding_0X634_bits[left_over_16] = unit_state[1];
       holding_0X644_bits[left_over_16] = unit_state[2];
       holding_0X654_bits[left_over_16] = unit_state[3];
   }

   foreach (modbus_slave *slave , *_modbuses)
       if(slave->serverAddress() == modbus_address)
       {
           qDebug() << "write data to :" << slave->serverAddress() << endl;
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X601,value_0X601);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X602,value_0X602);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X701,value_0X701);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X702,value_0X702);

           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X801,value_0X801);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X802,value_0X802);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X803,value_0X803);
           slave->setData(QModbusDataUnit::DiscreteInputs,address_discrete_0X804,value_0X804);

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X01,qBitArrayToQuint16(holding_0X01_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X11,qBitArrayToQuint16(holding_0X11_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X21,qBitArrayToQuint16(holding_0X21_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X501,qBitArrayToQuint16(holding_0X501_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X601,qBitArrayToQuint16(holding_0X601_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X602,qBitArrayToQuint16(holding_0X602_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X603,qBitArrayToQuint16(holding_0X603_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X604,qBitArrayToQuint16(holding_0X604_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X611,qBitArrayToQuint16(holding_0X611_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X612,qBitArrayToQuint16(holding_0X612_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X613,qBitArrayToQuint16(holding_0X613_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X614,qBitArrayToQuint16(holding_0X614_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X621,qBitArrayToQuint16(holding_0X621_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X622,qBitArrayToQuint16(holding_0X622_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X623,qBitArrayToQuint16(holding_0X623_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X624,qBitArrayToQuint16(holding_0X624_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X631,qBitArrayToQuint16(holding_0X631_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X632,qBitArrayToQuint16(holding_0X632_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X633,qBitArrayToQuint16(holding_0X633_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X634,qBitArrayToQuint16(holding_0X634_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X641,qBitArrayToQuint16(holding_0X641_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X642,qBitArrayToQuint16(holding_0X642_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X643,qBitArrayToQuint16(holding_0X643_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X644,qBitArrayToQuint16(holding_0X644_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X651,qBitArrayToQuint16(holding_0X651_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X652,qBitArrayToQuint16(holding_0X652_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X653,qBitArrayToQuint16(holding_0X653_bits));
           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X654,qBitArrayToQuint16(holding_0X654_bits));

           slave->setData(QModbusDataUnit::HoldingRegisters,address_holding_0X401,count_low_high);
       }
    }
    else
        qDebug() << "Mqtt Messafe data Not qulified Emulator Operation ...";

}

QBitArray mqttMessageManager::quint16ToQBitArray(quint16 v)
{
    QBitArray ba(16);
    for (int i=0; i<16; i++)
        ba.setBit(i, v>>i & 1);
    return ba;
}


quint16 mqttMessageManager::qBitArrayToQuint16(const QBitArray &ba)
{
    quint16 v = 0;
    for (int i=0; i<16; i++)
        v |= ba.at(i)<<i;
    return v;
}
