#include "modbus_slave.h"

modbus_slave::modbus_slave(QObject *parent) : QModbusRtuSerialSlave(parent)
{
    connect(this,SIGNAL(stateChanged(QModbusDevice::State)),this,SLOT(func1(QModbusDevice::State)));

    connect(this,SIGNAL(dataWritten(QModbusDataUnit::RegisterType,int,int))
            ,this,SLOT(func2(QModbusDataUnit::RegisterType,int,int)));

    connect(this,SIGNAL(errorOccurred(QModbusDevice::Error))
            ,this,SLOT(manipulateError(QModbusDevice::Error)));

}


void modbus_slave::setConnectionValues(QString port,int buadrate,int data_bits
                         ,int stop_bits,int _parity)
{
    port_name = port;
    this->setConnectionParameter(QModbusDevice::SerialPortNameParameter , port_name);
    this->setConnectionParameter(QModbusDevice::SerialBaudRateParameter,buadrate);
    this->setConnectionParameter(QModbusDevice::SerialDataBitsParameter,data_bits);
    this->setConnectionParameter(QModbusDevice::SerialParityParameter,_parity);
    this->setConnectionParameter(QModbusDevice::SerialStopBitsParameter,stop_bits);
}

void modbus_slave::setModbusMemoryMap(quint16 coil__start,quint16 coil__end
                        ,quint16 discrete__start ,quint16 discrete__end
                        ,quint16 input__start,quint16 input__end
                        ,quint16 holding__start,quint16 holding__end)
{
    QModbusDataUnitMap reg;

    reg.insert(QModbusDataUnit::DiscreteInputs, { QModbusDataUnit::DiscreteInputs, 0, 4096 });    // 4096  =0x1000
    reg.insert(QModbusDataUnit::HoldingRegisters, { QModbusDataUnit::HoldingRegisters, 0, 2048 });    // 2048 = 0x800

    this->setMap(reg);
}

void modbus_slave::func1(QModbusDevice::State state)
{
        qDebug () <<"********* status *********" << state;
}

void modbus_slave::func2(QModbusDataUnit::RegisterType t ,int i ,int c)
{
    qDebug () <<"********* data *********" << t;
}

void modbus_slave::manipulateError(QModbusDevice::Error error)
{
    qDebug () <<"********* error *********" << error;

}
