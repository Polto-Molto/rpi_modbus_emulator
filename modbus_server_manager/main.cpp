#include <QCoreApplication>
#include <modbus_slave.h>
#include <QSettings>
#include "mqttmessagemanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    QVector<modbus_slave*> *modbuses = new QVector<modbus_slave*>;


    QSettings *modbus_settings = new QSettings("modbus_settings.ini",QSettings::IniFormat);
    modbus_settings->beginGroup("main_mqtt");
    QString mqtt_ip = modbus_settings->value("ip").toString();
    QString mqtt_id = modbus_settings->value("id").toString();
    int mqtt_port = modbus_settings->value("port").toInt();
    QString mqtt_topic = modbus_settings->value("topic").toString();
    modbus_settings->endGroup();

    for(int i = 1 ;i < 33;i++)
    {
        modbus_slave *slave = new modbus_slave;
        QString groupName;
        groupName = "modbus_"+ QString::number(i);
        modbus_settings->beginGroup(groupName);
        slave->setServerAddress(modbus_settings->value("address").toInt());
        slave->setConnectionValues(
                    modbus_settings->value("port").toString()
                    ,modbus_settings->value("baud").toInt()
                    ,modbus_settings->value("data_bits").toInt()
                    ,modbus_settings->value("stop_bits").toInt()
                    ,modbus_settings->value("parity").toInt()
                    );

        slave->setModbusMemoryMap(
                    modbus_settings->value("coils_start").toUInt()
                    ,modbus_settings->value("coils_end").toUInt()
                    ,modbus_settings->value("discrete_start").toUInt()
                    ,modbus_settings->value("discrete_end").toUInt()
                    ,modbus_settings->value("input_start").toUInt()
                    ,modbus_settings->value("inoput_end").toUInt()
                    ,modbus_settings->value("holding_start").toUInt()
                    ,modbus_settings->value("holding_end").toUInt()
                    );

       // QVector<quint16> values;
      //  values << modbus_settings->value("address").toInt() << 10 << 0 << 100;
      //  QModbusDataUnit data(QModbusDataUnit::HoldingRegisters,0,values);



        //slave->setData(data);
        modbuses->push_back(slave);
        modbus_settings->endGroup();

    }

    foreach (modbus_slave *slave , *modbuses)
        qDebug() << slave->port_name << " :: "
                 << slave->serverAddress() << "::"
                 << slave->connectDevice();

    mqttMessageManager* mqtt = new mqttMessageManager;
    mqtt->setMqttConnectionData(mqtt_ip , mqtt_port , mqtt_id);
    mqtt->setModbusVectorPointer(modbuses);
    mqtt->connectToBroker();
    mqtt->setSubScripTopic(mqtt_topic);

    return a.exec();
}




