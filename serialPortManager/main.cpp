#include <QCoreApplication>
#include <mainserialportmanager.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    mainSerialPortManager * mainserial =  new mainSerialPortManager;
    mainserial->setMainSerial("/dev/ttyAMA0",QSerialPort::Baud9600,
                              QSerialPort::Data8,QSerialPort::OneStop,
                              QSerialPort::NoParity);

    QStringList serials_string;
    for(int i = 1 ; i < 65 ; i=i+2)
        serials_string << "/dev/ttyMB"+QString::number(i);

    mainserial->setSerials(serials_string,QSerialPort::Baud9600,
                              QSerialPort::Data8,QSerialPort::OneStop,
                              QSerialPort::NoParity);

    mainserial->startHunting();

    return a.exec();
}
