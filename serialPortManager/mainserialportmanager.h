#ifndef MAINSERIALPORTMANAGER_H
#define MAINSERIALPORTMANAGER_H

#include <QObject>
#include <QSerialPort>
#include <QVector>
#include <wiringPi.h>

class mainSerialPortManager : public QObject
{
    Q_OBJECT

public:
    explicit mainSerialPortManager(QObject *parent = 0);
    void setMainSerial(QString portName,QSerialPort::BaudRate
                       ,QSerialPort::DataBits , QSerialPort::StopBits,
                       QSerialPort::Parity);

    void setSerials(QStringList ports,QSerialPort::BaudRate
                       ,QSerialPort::DataBits , QSerialPort::StopBits,
                       QSerialPort::Parity);

signals:

public slots:
    void mainSerialDataComming();
    void serialDataComming();
    void startHunting();
    void setReceivingMode(qint64);
    bool Crc16In(QByteArray &QBABytes);

private:
    QSerialPort * main_serial;
    QVector<QSerialPort*> serials;
    int data_size;
    int data_written_count;
};

#endif // MAINSERIALPORTMANAGER_H
