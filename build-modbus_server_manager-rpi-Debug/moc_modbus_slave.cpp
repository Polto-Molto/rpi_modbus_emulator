/****************************************************************************
** Meta object code from reading C++ file 'modbus_slave.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../modbus_server_manager/modbus_slave.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modbus_slave.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_modbus_slave_t {
    QByteArrayData data[9];
    char stringdata0[120];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_modbus_slave_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_modbus_slave_t qt_meta_stringdata_modbus_slave = {
    {
QT_MOC_LITERAL(0, 0, 12), // "modbus_slave"
QT_MOC_LITERAL(1, 13, 5), // "func1"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 20), // "QModbusDevice::State"
QT_MOC_LITERAL(4, 41, 5), // "func2"
QT_MOC_LITERAL(5, 47, 29), // "QModbusDataUnit::RegisterType"
QT_MOC_LITERAL(6, 77, 15), // "manipulateError"
QT_MOC_LITERAL(7, 93, 20), // "QModbusDevice::Error"
QT_MOC_LITERAL(8, 114, 5) // "error"

    },
    "modbus_slave\0func1\0\0QModbusDevice::State\0"
    "func2\0QModbusDataUnit::RegisterType\0"
    "manipulateError\0QModbusDevice::Error\0"
    "error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_modbus_slave[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       4,    3,   32,    2, 0x0a /* Public */,
       6,    1,   39,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Int, QMetaType::Int,    2,    2,    2,
    QMetaType::Void, 0x80000000 | 7,    8,

       0        // eod
};

void modbus_slave::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        modbus_slave *_t = static_cast<modbus_slave *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->func1((*reinterpret_cast< QModbusDevice::State(*)>(_a[1]))); break;
        case 1: _t->func2((*reinterpret_cast< QModbusDataUnit::RegisterType(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->manipulateError((*reinterpret_cast< QModbusDevice::Error(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QModbusDataUnit::RegisterType >(); break;
            }
            break;
        }
    }
}

const QMetaObject modbus_slave::staticMetaObject = {
    { &QModbusRtuSerialSlave::staticMetaObject, qt_meta_stringdata_modbus_slave.data,
      qt_meta_data_modbus_slave,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *modbus_slave::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *modbus_slave::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_modbus_slave.stringdata0))
        return static_cast<void*>(const_cast< modbus_slave*>(this));
    return QModbusRtuSerialSlave::qt_metacast(_clname);
}

int modbus_slave::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QModbusRtuSerialSlave::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
