#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h> //ioctl() call defenitions
main()
{
   int fd;
   fd = open("/dev/ttyUSB1",O_RDWR | O_NOCTTY );//Open Serial Port
  
   int RTS_flag;
   RTS_flag = TIOCM_DTR;
   ioctl(fd,TIOCMBIS,&RTS_flag);//Set RTS pin
   getchar();
   ioctl(fd,TIOCMBIC,&RTS_flag);//Clear RTS pin
   close(fd);
}
