/****************************************************************************
** Meta object code from reading C++ file 'mainserialportmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../serialPortManager/mainserialportmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainserialportmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_mainSerialPortManager_t {
    QByteArrayData data[9];
    char stringdata0[122];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_mainSerialPortManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_mainSerialPortManager_t qt_meta_stringdata_mainSerialPortManager = {
    {
QT_MOC_LITERAL(0, 0, 21), // "mainSerialPortManager"
QT_MOC_LITERAL(1, 22, 21), // "mainSerialDataComming"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 17), // "serialDataComming"
QT_MOC_LITERAL(4, 63, 12), // "startHunting"
QT_MOC_LITERAL(5, 76, 16), // "setReceivingMode"
QT_MOC_LITERAL(6, 93, 7), // "Crc16In"
QT_MOC_LITERAL(7, 101, 11), // "QByteArray&"
QT_MOC_LITERAL(8, 113, 8) // "QBABytes"

    },
    "mainSerialPortManager\0mainSerialDataComming\0"
    "\0serialDataComming\0startHunting\0"
    "setReceivingMode\0Crc16In\0QByteArray&\0"
    "QBABytes"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_mainSerialPortManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x0a /* Public */,
       3,    0,   40,    2, 0x0a /* Public */,
       4,    0,   41,    2, 0x0a /* Public */,
       5,    1,   42,    2, 0x0a /* Public */,
       6,    1,   45,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Bool, 0x80000000 | 7,    8,

       0        // eod
};

void mainSerialPortManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        mainSerialPortManager *_t = static_cast<mainSerialPortManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->mainSerialDataComming(); break;
        case 1: _t->serialDataComming(); break;
        case 2: _t->startHunting(); break;
        case 3: _t->setReceivingMode((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 4: { bool _r = _t->Crc16In((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject mainSerialPortManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_mainSerialPortManager.data,
      qt_meta_data_mainSerialPortManager,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *mainSerialPortManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *mainSerialPortManager::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_mainSerialPortManager.stringdata0))
        return static_cast<void*>(const_cast< mainSerialPortManager*>(this));
    return QObject::qt_metacast(_clname);
}

int mainSerialPortManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
